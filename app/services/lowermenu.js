import Service from '@ember/service';

export default Service.extend({
    visiblemenu:true,
    visible(){
        this.set('visiblemenu',true)
    },
    invisible(){
        this.set('visiblemenu',false)
    }
});
