import DS from 'ember-data';

export default DS.Model.extend({
    rfc: DS.attr('string'),
    razonSocial: DS.attr('string'),
    email: DS.attr('string'),
    cp: DS.attr('number'),
    colonia: DS.attr('string'),
    calle: DS.attr('string'),
    numExt: DS.attr('string'),
    numInt: DS.attr('string')

});
