import Route from '@ember/routing/route';

export default Route.extend({
  queryParams: {
    source: {
      refreshModel: true
    }
  },
  model() {
    return this.store.findAll('catalogo-concepto').catch(()=>{
      return null
    })
  
  }
});
