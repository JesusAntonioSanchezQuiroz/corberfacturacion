import { inject as service } from '@ember/service';
// import { camelize } from 'ember-string';
// import { pluralize } from 'ember-inflector';
import FirebaseAdapter from 'emberfire/adapters/firebase';
// import FirebaseFlexAdapter from 'emberfire-utils/adapters/firebase-flex';
import CascadeDeleteMixin from 'ember-data-cascade-delete';

export default FirebaseAdapter.extend(CascadeDeleteMixin, {
	currentUser: service(),

	recordWillDelete: function recordWillDelete(/*store, record*/) {
		// here we are doing nothing. Emberfire try to update parent for belongTo relationship directly
		// on firebase, which would lead sometimes to an error as the child get updated, while we delete it
		// when the parent receive the loopback from firebase.
	},

	pathForType(modelName){
		let path = this._super(...arguments);

		if(modelName == 'user'){
			return path
		}

		let accountId = this.get('currentUser.model.account')
		return `accounts/${accountId}/${path}`;
	}

	// query(store, type, query = {}, recordArray){
	// 	let collection = camelize(pluralize(type.modelName));

	// 	query.path = `/users/${this.get('session.currentUser.uid')}/${collection}`;
	// 	query.isReference = true;

	// 	return this._super(store, type, query, recordArray);
	// }
});
