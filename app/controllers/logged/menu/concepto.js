import { get } from '@ember/object';
import Controller from '@ember/controller';

export default Controller.extend( {
    queryParams:['source','id'],
    source: null,
    id: null,

    unidades: [
        {clave:'ACR',tipo:'acre Acre'},
        {clave:'PD',tipo:'Almohadilla'},
        {clave:'AMP',tipo:'A amperio'},
        {clave:'XAE',tipo:'Aerosol'},
        {clave:'Gd',tipo:'Barril bruto'},
        {clave:'TC',tipo:'camion'},
        {clave:'48',tipo:'carga masiva'},
    ],
    conceptos: [
        {clave:'60102401',tipo:'Abacos'},
        {clave:'11171500',tipo:'Aceros basicos'},
        {clave:'56121000',tipo:'Mobiliario de biblioteca'},
        {clave:'53111605',tipo:'Zapatos para bebe'},
        {clave:'56101515',tipo:'Camas'},
        {clave:'31162007',tipo:'Clavos de tapiceria'},
        {clave:'32111514',tipo:'Diodo optico'},
    ],
    actions:{
        setClaveUnidad(obj){
            if (obj) {
                this.set('model.claveUnidad', get(obj, 'clave'));
                this.set('model.claveUnidadSelected', obj);
              } else {
                this.set('model.claveUnidad', null);
                this.set('model.claveUnidadSelected', null);
              }
        },
        setClaveConcepto(obj){
            if (obj) {
                this.set('model.clave', get(obj, 'clave'));
                this.set('model.claveUnidadConcepto', obj);
              } else {
                this.set('model.clave', null);
                this.set('model.claveUnidadConcepto', null);
              }
        },
        nuevoImpuesto(tipo) {
            this.get('model.impuestos').createRecord({
              base:0,
              impuesto: 100,
              tipo: tipo
            });
          },
        save(){
            var concepto=this.get('model')
            debugger
            console.log(concepto)
            concepto.save()
        }
    }

});
