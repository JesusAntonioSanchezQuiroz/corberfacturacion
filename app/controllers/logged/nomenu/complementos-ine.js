import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        backC_ine() {
            this.transitionToRoute('logged.nomenu.impuestosGlobales')
        },
        nextC_ine() {
            this.transitionToRoute('logged.nomenu.resumen')
        }
    }
});
