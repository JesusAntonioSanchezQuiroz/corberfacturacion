import { makeArray } from '@ember/array';
import { isBlank, isPresent } from '@ember/utils';
import { oneWay } from '@ember/object/computed';
import { computed, get } from '@ember/object';
import { inject as service } from '@ember/service';
import DS from 'ember-data';
import Controller from '@ember/controller';
import { validator, buildValidations } from "ember-cp-validations";

const Validations = buildValidations({
    'rfc': [
        validator('presence', {
            presence: true,
            message: 'El campo RFC debe estar lleno'
        }),
        validator('format', {
            regex: /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
            message: 'El RFC no tiene el formato correcto'

        })
    ]
})

export default Controller.extend(Validations, {
    store: service(),
	catalogoSat: service(),
	shouldBeOpen: false,

	'-receptoresList': computed(function() {
		return this.get('store').findAll('fiscalReceptor');
	}),
	receptoresList: computed('rfcOptions.[]', '-receptoresList.[]', function() {
		return DS.PromiseArray.create({
			promise: this.get('-receptoresList').then((receptores) => {
				let rfcOptions = this.get('rfcOptions');
				if (isPresent(rfcOptions)) {
					return receptores.filter((receptor) => {
						return rfcOptions.contains(receptor.get('rfc'));
					});
				} else {
					return receptores;
				}
			})
		});
    }),

	'-usoCfdiObj': computed('model.usoCfdi', function() {
		return DS.PromiseObject.create({
			promise: this.get('catalogoSat').getDocument({
				type: 'c_UsoCFDI',
				id: this.get('model.usoCfdi')
			})
		});
	}),
	usoCfdiObj: oneWay('-usoCfdiObj'),


	'-monedaObj': computed('model.moneda', function() {
		return DS.PromiseObject.create({
			promise: this.get('catalogoSat').getDocument({
				type: 'c_Moneda',
				id: this.get('model.moneda')
			})
		});
	}),
	monedaObj: oneWay('-monedaObj'),


	'-formaPagoObj': computed('model.formaPago.[]', function() {
		if (isBlank(this.get('model.formaPago'))) {
			return [];
		}

		return DS.PromiseObject.create({
			promise: this.get('catalogoSat').getDocuments({
				type: 'c_FormaPago',
				body: {
					ids: this.get('model.formaPago')
				}
			}).then((arr)=> arr.get('firstObject'))
		});
	}),
	formaPagoObj: oneWay('-formaPagoObj'),


	formaPagoSort(a, b) {
		return Number(get(a, 'clave')) - Number(get(b, 'clave'));
	},
    emisoresWithCertificados: computed(function() {
		return this.get('store').findAll('fiscalEmisor');
	}),
    
    actions: {
        toogleError(attr) {
            switch (attr) {
                case 'rfc':
                    this.set('rfcErrorCheck', true)
                    break

            }
        },
        shouldBeOpen(element){
            return element.isActive;
        },

        onKeyDown(text, options, event){
            event.currentTarget.value = event.currentTarget.value.toUpperCase();
        },

        setReceptor(option) {
            this.set('model.receptor', option);
        },

        setUsoCfdi(option) {
            this.set('usoCfdiObj', option);
            this.set('model.usoCfdi', (option) ? get(option, 'clave') : null);
        },

        setFormaPago(opts) {
            this.set('formaPagoObj', opts);
            let options = makeArray(opts);
            this.set('model.formaPago', isPresent(options) ? options.mapBy('clave') : []);
        },

        setMoneda(option) {
            this.set('monedaObj', option);
            this.set('model.decimals', get(option, 'decimals'))
            this.set('model.moneda', (option) ? get(option, 'clave') : null);
            this.set('model.tipoCambio', null);
        },

        customSuggestion(term) {
            return `Presiona ENTER para facturarle a "${term.toUpperCase()}"...`;
        },

        createFiscalReceptor(rfc) {
            let receptor = this.get('store').createRecord('fiscalReceptor', {
                rfc: rfc,
                email: ''
            });
            receptor.get('fiscalDirecciones').createRecord({});
            this.set('model.receptor', receptor);

            this.$('.ember-power-select-typeahead-input, .ember-power-select-search-input').blur();

            this.$('#receptorModal').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
            this.$('#receptorModal').focus();
        },

         nextEmisor() {
            this.transitionToRoute('logged.nomenu.datosFactura')
         }
    }

});
