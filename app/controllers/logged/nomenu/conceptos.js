import Controller from '@ember/controller';
import ImpuestosGlobalesHandler from 'ember-cli-facturacion-logic/mixins/facturas/handlers/impuestos-globales';


export default Controller.extend(ImpuestosGlobalesHandler,{
    actions: {
        backConceptos() {

            this.transitionToRoute('logged.nomenu.datosFactura')
        },
        nextConceptos() {

            this.transitionToRoute('logged.nomenu.impuestosGlobales')
        }
    }
});
