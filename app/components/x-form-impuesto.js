import { oneWay, empty, or } from '@ember/object/computed';
import { isBlank, isPresent } from '@ember/utils';
import { computed, get } from '@ember/object';
import Component from '@ember/component';
import {
  grouped,
  C_IMPUESTO_IEPS,
  TRASLADADO,
  RETENCION,
  FACTOR_CUOTA,
  FACTOR_TASA
} from 'ember-cli-facturacion-logic/utils/catalogos/impuestos';

const entradas = ["Elección", "Personalizado"]

// const BreakException = {};

export default Component.extend({
  entradas: entradas,
  tipoTrasladado: TRASLADADO,
  tipoRetencion: RETENCION,
  groupedImpuestos: computed('locales', function() {
    grouped.forEach((obj)=>{
      let options = get(obj, 'options');
      if(!isBlank(options)){
        
        options.filterBy('local').invoke('set', 'disabled', !this.get('locales'));
      }
    });
    return grouped;
  }),
  locales: true,
  showDeleteBtn: true,
  '-impuestoObj': computed('impuesto.{impuesto,tipo}', {
    get() {
      let impuesto = this.get('impuesto');
      let group = this.get('groupedImpuestos').findBy('tipo', impuesto.get('tipo'));

      if(impuesto.get('local')){
        return get(group, 'options').findBy('id', 'Local');
      }

      if (group && get(group, 'options')) {
        return get(group, 'options').findBy('id', impuesto.get('impuesto'));
      } else {
        return {};
      }
    }
  }),
  impuestoObj: oneWay('-impuestoObj'),
  showExentoBtn: computed('impuesto.{tipo,impuesto,tipoFactor}',function(){
    return (this.get('impuesto.impuesto')=="002")&&(this.get('impuesto.tipo')==2)
  }),

  allowRange: computed('impuestoObj.inputTypes.[]', function() {
    let inputTypes = this.get('impuestoObj.inputTypes');
    if (isPresent(inputTypes)) {
      return inputTypes.includes('range');
    }
  }),
  allowFixed: computed('impuestoObj.inputTypes.[]', function() {
    let inputTypes = this.get('impuestoObj.inputTypes');
    if (isPresent(inputTypes)) {
      return inputTypes.includes('fixed');
    }
  }),
  _showRangeInput: computed(function(){
    return isPresent(this.get('impuesto.tasaCuota')) && !(this.get('impuestoObj.fixedValues') || []).includes(`${(this.get('impuesto.tasaCuota')/100).toFixed(6)}`)
  }),
  emptyFixedValues: empty('impuestoObj.fixedValues'),
  showRangeInput: or('emptyFixedValues', '_showRangeInput'),

  defaultMax: 100,
  defaultMin: 0,
  rangeInputMax: or('impuestoObj.range.max', 'defaultMax'),
  rangeInputMin: or('impuestoObj.range.min', 'defaultMin'),
  rangeInputSuffix: computed('impuesto.isTasa', function() {
    return (this.get('impuesto.isTasa')) ? "%" : '';
  }),
  rangeInputComponent: computed('impuesto.{isCuota,isTasa}', function() {
    if (this.get('impuesto.isCuota')) {
      return 'currency-input'
    }
    if (this.get('impuesto.isTasa')) {
      return 'number-input'
    }
  }),
  init(){
    this._super(...arguments)
    this.set('tipoEntrada', "Elección")
    this.set('exento', false)
  },

  actions: {
    borrarImpuesto(impuesto) {
      impuesto.deleteRecord();
      this.sendAction('didDestroyImpuesto', impuesto);
    },

    setTipoImpuesto(value) {
      this.setProperties({
        'impuesto.impuesto': (value && !get(value, 'local')) ? get(value, 'id') : null,
        'impuesto.tipo': value ? get(value, 'tipo') : null,
        'impuestoObj': value,
        'impuesto.tasaCuota': null,
        'impuesto.local': value ? !!get(value, 'local') : false,
        'impuesto.tipoFactor': 'Tasa'
      });
      this.set('exento', false)
    },

    toggleRange(option) {
      let value = false
      if(option=="1"){
        value=true
        this.set('exento', false)
        if (this.get('impuesto.impuesto') == C_IMPUESTO_IEPS) {
          this.set('impuesto.tipoFactor', FACTOR_CUOTA);
        } else {
            this.set('impuesto.tipoFactor', FACTOR_TASA);
        }
        
      }
      else if(option=="0"){
         this.set('exento', false)
         if (value && this.get('impuesto.impuesto') == C_IMPUESTO_IEPS) {
              this.set('impuesto.tipoFactor', FACTOR_CUOTA);
          } else {
              this.set('impuesto.tipoFactor', FACTOR_TASA);
          }
      }else {     
        this.set('exento', true)
        this.set('impuesto.tipoFactor', 'Exento');
        this.set('impuesto.tasaCuota', null);

      }
      
      this.set('_showRangeInput', value);
     
    },

    recalculateImporte(){
      let impuesto = this.get('impuesto');

      impuesto.set('importeCustom', null);
      impuesto.notifyPropertyChange('importe');
    }
  }

});
