import { computed, get } from '@ember/object';
import {makeArray} from '@ember/array';
import { inject as service } from '@ember/service';
import { oneWay } from '@ember/object/computed';
import Component from '@ember/component';
// import DS from 'ember-data';

export default Component.extend({
  store: service(),

  showDescuentoField: true,
  showDeleteBtn: true,

  busquedaClaveProducto: false,
  classNames: 'concepto'.w(),
  isParte: oneWay('concepto.isParte'),
  editable: computed('factura.editable', function(){
    if(this.get('factura')){
      return this.get('factura.editable');
    }
    return true;
  }),

  initialOptions: computed('concepto.claveSelected', function(){
    return makeArray(this.get('concepto.claveSelected'));
  }),

  conceptosQuery: computed(function() {
    return function(term) {
      return {
        query: {
          "query_string": {
            "fields": ["name", "clave^5"],
            "query": `${term}*`
          }
        },
        sort: [
          {"name.keyword": "asc"}
        ],
        size: 10000
      }
    }
  }),


  didInsertElement() {
    this._super(...arguments);

    this.$('[data-toggle="tooltip"]').tooltip();
    this.$('.dropdown-toggle').dropdown();
    this.$().on('click', '.dropdown-menu', function(e) {
      e.stopPropagation();
    });
  },

  actions: {
    toggleBusquedaClaveProducto(value) {
      this.set('busquedaClaveProducto', value);
    },

    nuevoImpuesto(tipo) {
      this.get('concepto.impuestos').createRecord({
        base:0, // TODO: verificar si esto es válido
        tipo: tipo
      });
    },

    nuevaParte() {
      this.get('concepto.partes').createRecord({
        isParte: true,
        isCatalogo: true
      });
    },

    setClaveProdServActivada(obj) {
      if (obj) {
        this.set('concepto.clave', get(obj, 'clave'));
        this.set('concepto.claveSelected', obj);
      } else {
        this.set('concepto.clave', null);
        this.set('concepto.claveSelected', null);
      }
    },

    borrarConcepto(concepto) {
      if (concepto.get('isNew')) {
        concepto.deleteRecord();
      } else {
        concepto.set('_destroy', true);
      }
    }
  }
});
