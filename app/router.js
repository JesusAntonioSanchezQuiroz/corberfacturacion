import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');
  this.route('index');
  this.route('logged', {path: '/'}, function() {
    this.route('nomenu',{path: 'facturar/:f_id'}, function() {
      this.route('emisorReceptor', {path: '/'});
      this.route('datosFactura');
      this.route('conceptos');
      this.route('complementosIne');
      this.route('resumen');
      this.route('impuestosGlobales');
    });
    this.route('menu',{path: '/'}, function() {
      this.route('factura',{path: '/'});
      this.route('altaclientes');
      this.route('canceladas');
      this.route('catalogoconceptos');
      this.route('conceptoseimpuestos');
      this.route('detallefactura');
      this.route('listaclientes');
      this.route('usuario');
      this.route('editarcliente',{path:'editarcliente/:receptor_id'});
      this.route('concepto');
      this.route('editarconcepto',{path:'editarconcepto/:concepto_id'});
    });
  });
});

export default Router;


